import AboutPage from './component/AboutPage';
import Header from './component/common/Header';
import HomePage from './component/HomePage';
import { Route, Switch, Redirect} from 'react-router-dom';
import NotFoundPage from './component/NotFoundPage';
import ConferencePage from './component/conference/ConferencePage';

function App() {
  return (
    <div className="container-md">
    <Header></Header>
    <Switch>
      <Route path='/' exact component={HomePage} ></Route>
      <Route path='/about' component={AboutPage} ></Route>
      <Route path='/conferences' component={ConferencePage} ></Route>
      <Redirect from='/acerca-de' to='about'></Redirect>
      <Route component={NotFoundPage}></Route>
    </Switch>

    </div>
  );
}

export default App;
