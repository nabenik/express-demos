const baseUrl = 'http://localhost:4001/api/sessions';

export default class SessionAPI{

    getConferences(){
        return fetch(baseUrl)
            .then(this.handleResponse)
            .catch(this.handleError);
    }

    async handleResponse(response) {
        if(response.ok) return response.json();
        if(response.status === 400) {
            const error = await response.text();
            throw new Error(error);
        }
        throw new Error('La respuesta no es correcta');
    }

    handleError(error){
        console.error('Hubo un error ' + error);
        throw error;
    }

}