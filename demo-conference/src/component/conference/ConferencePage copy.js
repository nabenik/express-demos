import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as conferenceActions from '../../redux/actions/conferenceActions';
class componentName extends Component {
    state = {
        conference: {
            title: ''
        }
    }

    handleChange = event => {
        const conference = {...this.state.conference, title: event.target.value}
        this.setState( {conference})
    }

    handleSubmit = event => {
        event.preventDefault();
        // despachar la accion para que llegue a redux
        this.props.createConference(this.state.conference);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h2>Conferencia</h2>
                <h3>Agregar conferencia</h3>
                <input
                type="text"
                value={this.state.conference.title}
                onChange={this.handleChange}
                ></input>
                <input type="submit" value="Guardar"/>
                {this.props.conferences.map( conference => (
                    <div key={conference.title}>{conference.title}</div>
                ))}
            </form>
        );
    }
}

//State {conferencias, autores, pagos, partipaciones especiales}
function mapStateToProps(state){
    return {
        conferences: state.conferences
    }
}

//Voy a (filtrar y) mapear del universo de acciones, las que le conciernen al componente
/*function mapDispatchToProps(dispatch){
    return {
        createConference: conference => dispatch(conferenceActions.createConference(conference))
    }
}*/

const mapDispatchToProps = {
    createConference: conferenceActions.createConference
}

export default connect(mapStateToProps, mapDispatchToProps)(componentName);