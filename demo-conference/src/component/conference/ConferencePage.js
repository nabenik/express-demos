import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as conferenceActions from '../../redux/actions/conferenceActions';

function ConferenceList({conferences = []}){
    return (
        <table className='table'>
            <thead>
                <th>Id</th>
                <th>Title</th>
                <th>Description</th>
            </thead>
            <tbody>
                {conferences.map(conference => {
                    return (
                        <tr key={conference.id}>
                            <td>{conference.id}</td>
                            <td>{conference.title}</td>
                            <td>{conference.description.slice(0,10)}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
}

class ConferencePage extends Component {

    componentDidMount(){
        this.props.loadConferences().catch(error => {
            alert(' Hubo un error en la comunicacion ' + error)
        });
    }

    render() {
        return (
            <>
                <h2>Conferencia</h2>
                <ConferenceList conferences={this.props.conferences}></ConferenceList>
            </>
        );
    }
}

//State {conferencias, autores, pagos, partipaciones especiales}
function mapStateToProps(state){
    return {
        conferences: state.conferences
    }
}

//Voy a (filtrar y) mapear del universo de acciones, las que le conciernen al componente
/*function mapDispatchToProps(dispatch){
    return {
        createConference: conference => dispatch(conferenceActions.createConference(conference))
    }
}*/

const mapDispatchToProps = {
    createConference: conferenceActions.createConference,
    loadConferences: conferenceActions.loadConferences
}

export default connect(mapStateToProps, mapDispatchToProps)(ConferencePage);