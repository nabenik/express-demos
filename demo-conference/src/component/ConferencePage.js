import React, { useState, useEffect } from 'react';
import SessionAPI from '../api/sessionApi';

function ConferenceList({conferences = []}){
    return (
        <table className='table'>
            <thead>
                <th>Id</th>
                <th>Title</th>
                <th>Description</th>
            </thead>
            <tbody>
                {conferences.map(conference => {
                    return (
                        <tr key={conference.id}>
                            <td>{conference.id}</td>
                            <td>{conference.title}</td>
                            <td>{conference.description.slice(0,10)}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
}


export default function ConferencePage(){
    const [conferences, setConferences] = useState([]);

    useEffect(() => {
        const api = new SessionAPI();
        api.getConferences().then(_confs => setConferences(_confs))
            .catch(error => console.log(error));
    }, []);

    return (
        <>
            <h1>Conferencias</h1>
            <ConferenceList conferences={conferences}></ConferenceList>
        </>
    )
}