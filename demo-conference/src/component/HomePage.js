import React from "react";

const HomePage = () => (
    <div className="jumbotron">
        <h1>JSConf 2021</h1>
        <p>JavaScript, TypeScript, Dart y relacionados</p>
        <br></br>
        <p>La mayor conferencia JS de Guatemala</p>
        <a href='/about' className="btn btn-primary">About</a>
    </div>
)

export default HomePage;