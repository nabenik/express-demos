import React from 'react';
import { NavLink } from 'react-router-dom';

export default function Header(){
    return (
        <nav>
            {/*<a href="/">Principal</a> | 
            <a href="/about">Acerca de</a>*/}
            <NavLink exact to='/'>Principal</NavLink> {'|'}
            <NavLink to='/conferences'>Conferencias</NavLink> {'|'}
            <NavLink to='/about'>Acerca de</NavLink>
        </nav>
    )
}