import {CREATE_CONFERENCE, LOAD_CONFERENCES_SUCCESS} from './actionTypes'
import SessionApi from '../../api/sessionApi'

export function createConference(conference){
    return {type: CREATE_CONFERENCE, conference}
}


export function loadConferences(){
    return function(dispatch) {
        const api = new SessionApi();
        return api.getConferences()
            .then(conferences => {
                dispatch( { type: LOAD_CONFERENCES_SUCCESS, conferences})
            }) //Publicar en redux el exito
            .catch(error => {
                throw error;
            }); //Publicar en redux el fracaso;
    }
}
