import { combineReducers}  from 'redux';
import conferences from './conferenceReducer';

const rootReducer = combineReducers({
    conferences
});

export default rootReducer;