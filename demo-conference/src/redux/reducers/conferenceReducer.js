import { CREATE_CONFERENCE, LOAD_CONFERENCES_SUCCESS } from "../actions/actionTypes";

export default function conferenceReducer(state = [], action){

    switch(action.type) {
        case CREATE_CONFERENCE:
            //Manipular el estado
            return [...state, {...action.conference}];
        case LOAD_CONFERENCES_SUCCESS:
            return action.conferences;
        default:
            return state;
    }
}