import express from "express";
import chalk from "chalk";
import debug from "debug";
import morgan from "morgan";

import path, { dirname } from "path";
import { fileURLToPath } from "url";
import conferenceRouter from "./src/routers/conferenceRouter.js";
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const app = express();
const PORT = process.env.PORT || 3000;
const globalDebug = debug("appmp");

app.use(express.static(path.join(__dirname, '/public/')));

app.set('views', './src/views');
app.set('view engine', 'ejs');

app.use('/sessions', conferenceRouter);

app.get('/', (req, res) => {
    res.render("index",
        {
            'title':'JSConf 2021',
            'lenguajes': ['JavaScript','TypeScript',
            'CoffeeScript', 'Dart', 'Clojure.js']
        });
    }
);


app.use(morgan('common'));

app.listen(PORT, () => {
    globalDebug(`Express ha arrancado en el puerto ${chalk.green(PORT)}`);
});

