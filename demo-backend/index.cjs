const http = require('http');
//import http from "http";

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end('Hola mundo en node \n');
    }
);

server.listen(port, hostname, () =>{
    console.log(`Mi servidor se está ejecutando en la puerta ${port}`);
})

// Shock absorver (NGINX - JSON) -> Node server
// PM2 (control de procesos) -> Node
// Docker