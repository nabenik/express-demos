import express from 'express';
import { default as sessionsData } from '../data/sessions.json'

const conferenceRouter = express.Router();

conferenceRouter.route("/")
    .get((req, res) => {
        res.render("sessions",
            {
                'title':'JSConf 2021',
                'lenguajes': ['JavaScript','TypeScript',
                'CoffeeScript', 'Dart', 'Clojure.js'],
                'sessions': sessionsData
            });
        }
    );

conferenceRouter.route('/:id')
    .get((req, res) => {
        const id = req.params.id;
        const session = sessionsData.find(s => s.id == id);
        res.render("session", { 'session': session});
    })

export default conferenceRouter;