import mongoose from 'mongoose';

const { Schema } = mongoose;

const Session = mongoose.model('Session', Schema({
    id: Number,
    title: String,
    description: String
}));

export default Session;