import mongoose from 'mongoose';

const { Schema } = mongoose;

const Book = mongoose.model('Book', Schema({
    title: String,
    author: String,
    genre: String,
    read: Boolean
}, { collection: 'book'}));

export default Book;