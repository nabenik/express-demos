import express from 'express';

export default function sessionRouter(Session){
    const sessionRouter = express.Router();

    sessionRouter.route('/sessions')
        .post((req, res)=>{
            const session = new Session(req.body);//String
            session.save();
            return res.status(201).json(session);  
        })
        .get((req, res) => {
            const { query } = req;

            Session.find(query, (err, sessions) => {
                if (err) {
                    return res.send(err);
                }
                return res.json(sessions);
            });
        });

    sessionRouter.use('/sessions/:sessionId', (req, res, next)=> {
        const sessionId = req.params.sessionId;
        Session.findById(sessionId, (err, session)=>{
            if(err){
                return res.send(err);
            }
            if(session){
                req.session = session;
                return next();
            }
            return res.sendStatus(404);
        });
    });

    sessionRouter.route('/sessions/:sessionId')
        .get((req,res) => res.json(req.session))
        .patch((req, res)=>{
            const {session} = req;//Mongo

            Object.entries(req.body).forEach((item)=>{
                const key = item[0];
                const value = item[1];
                session[key] = value;
            });

            session.save((err)=>{
                if(err){
                    return res.send(err);
                }
                return res.json(session);
            });
        })
        .delete(async (req, res)=>{
            try {
                const {session} = req;
                await session.remove();
                return res.sendStatus(204);    
            } catch (error) {
                return res.send(error);                
            }
        })
        .put((req,res) =>  {
            Session.findByIdAndUpdate(req.params.sessionId, req.body, (err, session) =>{
                if(err) return res.send(err);
                return res.json(session);
            })
        });
    
    return sessionRouter;
}

