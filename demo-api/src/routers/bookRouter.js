import express from 'express';

export default function bookRouter(Book){
    const bookRouter = express.Router();

    //Head (llave:valor ) - Body (texto)
    bookRouter.route('/books')
        .post((req, res)=>{
            const book = new Book(req.body);//String
            book.save();
            return res.status(201).json(book);  
        })
        .get((req, res) => {
            const { query } = req;

            Book.find(query, (err, books) => {
                if (err) {
                    return res.send(err);
                }
                return res.json(books);
            });
        });


    // ------ Node ----- Express (interceptor1, interceptor2, interceptor 3) ----- Callback de respuesta

    // 1- Analizar el parametro BookId
    // 2- Buscar la entidad en mongo
    // 3- Pasar en la secuencia la entidad encontrada

    bookRouter.use('/books/:bookId', (req, res, next)=> {
        const bookId = req.params.bookId;
        Book.findById(bookId, (err, book)=>{
            if(err){
                return res.send(err);
            }
            if(book){
                req.book = book;
                return next();
            }
            return res.sendStatus(404);
        });
    });

    bookRouter.route('/books/:bookId')
        .get((req,res) => res.json(req.book))
        .patch((req, res)=>{
            const {book} = req;//Mongo

            Object.entries(req.body).forEach((item)=>{
                const key = item[0];
                const value = item[1];
                book[key] = value;
            });

            book.save((err)=>{
                if(err){
                    return res.send(err);
                }
                return res.json(book);
            });
        })
        .delete(async (req, res)=>{
            try {
                const {book} = req;
                await book.remove();
                return res.sendStatus(204);    
            } catch (error) {
                return res.send(error);                
            }
        })
        .put((req,res) =>  {
            Book.findByIdAndUpdate(req.params.bookId, req.body, (err, book) =>{
                if(err) return res.send(err);
                return res.json(book);
            })
        });
    /*bookRouter.route('/books/:bookId')
        .get((req,res)=>{
            const bookId = req.params.bookId;
            Book.findById(bookId, (err, book)=>{
                if(err){
                    return res.send(err);
                }
                if(book){
                    return res.json(book);
                }
                return res.sendStatus(404);
            })

        })
        .put((req,res) =>  {
            Book.findByIdAndUpdate(req.params.bookId, req.body, (err, book) =>{
                if(err) return res.send(err);
                return res.json(book);
            })
        })*/
    return bookRouter;
}

