import express from 'express';
import debug from 'debug';
import chalk from 'chalk';
import mongoose from 'mongoose';
import Book from './src/models/bookModel.js';
import bookRouter from './src/routers/bookRouter.js';
import Session from './src/models/sessionModel.js'
import sessionRouter from './src/routers/sessionRouter.js';
import bodyParser from 'body-parser';
import cors from 'cors';

const app = express();
const globalDebug = debug("appmp")
const db = mongoose.connect('mongodb://localhost:27017/sessionsAPI')
const PORT = process.env.PORT || 3000;

mongoose.set('debug', true);
app.use(cors());
//Middleware usan un orden
app.use(bodyParser.urlencoded({ extended:false}));
app.use(bodyParser.json());

app.use('/api', bookRouter(Book));
app.use('/api', sessionRouter(Session));

app.listen(PORT, () => {
    globalDebug(`listening on port ${chalk.green(PORT)}`)
})

